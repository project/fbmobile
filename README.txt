
This module extends the Fbconnect module. Fbmobile provides an alternative
login button for mobile devices. All it currently does is override the 
theme_fbconnect_login_button() for mobile device users and provides a custom 
html button for either wap or touch (modern) phones. 

INSTALLATION

- install as usual

- depends on Fbconnect
  http://drupal.org/project/fbconnect

- depends on Mobile Tools
  http://drupal.org/project/mobile_tools
  
CONFIGURATION

There is 1 configuration option in /admin/settings/fbconnect/apperance

Choose the mobile mode: Touch or WAP. WAP supports all devices, while 
Touch is better-looking but ony works on modern (Full HTML) devices.